﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Rook.State;

namespace Rook
{
	[Flags]
	public enum Alignments
	{
		Center = 0,
		Left = 1,
		Right = 2,
		Top = 4,
		Bottom = 8
	}

	public class MainGame : Game
	{
		private GraphicsDeviceManager graphics;
		private SpriteBatch spriteBatch;
		private SuperBatch superBatch;
		private GameLoop gameLoop;
		private Camera camera;

		public MainGame()
		{
			graphics = new GraphicsDeviceManager(this);
			Content.RootDirectory = "Content";
			Window.Position = new Point(100);
			IsMouseVisible = true;
		}
		
		protected override void Initialize()
		{
			camera = new Camera();
			gameLoop = new GameplayLoop();
			gameLoop.Initialize(camera);

			base.Initialize();
		}
		
		protected override void LoadContent()
		{
			spriteBatch = new SpriteBatch(GraphicsDevice);
			superBatch = new SuperBatch(camera, spriteBatch, GraphicsDevice);
		}
		
		protected override void Update(GameTime gameTime)
		{
			float dt = (float)gameTime.ElapsedGameTime.Milliseconds / 1000;

			camera.Update(dt);
			gameLoop.Update(dt);
		}
		
		protected override void Draw(GameTime gameTime)
		{
			GraphicsDevice.Clear(Color.Black);
			gameLoop.Draw(superBatch);
		}
	}
}
