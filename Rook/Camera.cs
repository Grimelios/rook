﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Rook.Interfaces;

namespace Rook
{
	public class Camera : IPositionable, IRotatable, IDynamic
	{
		public Camera()
		{
			Transform = Matrix.Identity;
		}

		public Vector2 Position { get; set; }
		public Vector2 Origin { get; set; }
		public Matrix Transform { get; private set; }

		public float Rotation { get; set; }

		public void Update(float dt)
		{
			Transform = Matrix.CreateRotationZ(Rotation) * Matrix.CreateTranslation(new Vector3(Origin - Position, 0));
		}
	}
}
