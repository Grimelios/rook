﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rook.Core;

namespace Rook.Interfaces
{
	public interface IBoundable
	{
		Bounds Bounds { get; }
	}
}
