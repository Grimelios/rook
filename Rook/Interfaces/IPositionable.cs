﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace Rook.Interfaces
{
	public interface IPositionable
	{
		Vector2 Position { get; set; }
	}
}
