﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Rook.Interfaces;

namespace Rook
{
	public static class Functions
	{
		public static int EnumCount<T>()
		{
			return Enum.GetValues(typeof(T)).Length;
		}

		public static float ComputeAngle(Vector2 vector)
		{
			return ComputeAngle(Vector2.Zero, vector);
		}

		public static float ComputeAngle(Vector2 start, Vector2 end)
		{
			float dX = end.X - start.X;
			float dY = end.Y - start.Y;

			return (float)Math.Atan2(dY, dX);
		}

		public static void PositionItems<T>(T[] items, Vector2 basePosition, Vector2 spacing) where T : IPositionable
		{
			for (int i = 0; i < items.Length; i++)
			{
				items[i].Position = basePosition + spacing * i;
			}
		}

		public static Vector2 ComputeOrigin(Point dimensions, Alignments alignment)
		{
			return ComputeOrigin(dimensions.X, dimensions.Y, alignment);
		}

		public static Vector2 ComputeOrigin(int width, int height, Alignments alignment)
		{
			bool left = (alignment & Alignments.Left) == Alignments.Left;
			bool right = (alignment & Alignments.Right) == Alignments.Right;
			bool top = (alignment & Alignments.Top) == Alignments.Top;
			bool bottom = (alignment & Alignments.Bottom) == Alignments.Bottom;

			int x = left ? 0 : (right ? width : width / 2);
			int y = top ? 0 : (bottom ? height : height / 2);

			return new Vector2(x, y);
		}
	}
}
