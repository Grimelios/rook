﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rook
{
	using Receiver = Action<object, float>;

	public enum MessageTypes
	{
		Keyboard,
		Mouse,
		Xbox360,
		Input,
		Exit
	}

	public static class Messaging
	{
		private static List<Receiver>[] receivers = new List<Receiver>[Functions.EnumCount<MessageTypes>()];

		public static void Subscribe(MessageTypes messageType, Receiver action)
		{
			VerifyList(messageType).Add(action);
		}

		public static void Unsubscribe(MessageTypes messageType, Receiver action)
		{
			receivers[(int)messageType].Remove(action);
		}

		public static void Send(MessageTypes messageType, object data, float dt = 0)
		{
			VerifyList(messageType).ForEach(a => a(data, dt));
		}

		private static List<Receiver> VerifyList(MessageTypes messageType)
		{
			int index = (int)messageType;

			return receivers[index] ?? (receivers[index] = new List<Receiver>());
		}
	}
}
