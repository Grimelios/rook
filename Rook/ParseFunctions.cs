﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace Rook
{
	public static class ParseFunctions
	{
		public static Color ParseColor(string value)
		{
			string[] tokens = value.Split(',');

			// Single values are assumed to be grayscale.
			if (tokens.Length == 1)
			{
				int lightness = int.Parse(tokens[0]);

				return new Color(lightness, lightness, lightness, 255);
			}

			int r = int.Parse(tokens[0]);
			int g = int.Parse(tokens[1]);
			int b = int.Parse(tokens[2]);
			int a = tokens.Length == 3 ? 255 : int.Parse(tokens[3]);

			return new Color(r, g, b, a);
		}

		public static Vector2 ParseVector2(string value)
		{
			string[] tokens = value.Split(',');

			float x = float.Parse(tokens[0]);
			float y = float.Parse(tokens[1]);

			return new Vector2(x, y);
		}

		public static T ParseEnum<T>(string value)
		{
			return (T)Enum.Parse(typeof(T), value);
		}
	}
}
