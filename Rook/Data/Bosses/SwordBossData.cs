﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rook.Data.Bosses
{
	public class SwordBossData : BossData
	{
		public int PlatformLength { get; set; }
		public int PlatformDuration { get; set; }
	}
}
