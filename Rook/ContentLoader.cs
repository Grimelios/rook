﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Rook
{
	public static class ContentLoader
	{
		private static ContentManager content;

		public static void Initialize(ContentManager c)
		{
			content = c;
		}

		public static SpriteFont LoadFont(string font)
		{
			return content.Load<SpriteFont>("Fonts/" + font);
		}

		public static Texture2D LoadTexture(string texture)
		{
			return content.Load<Texture2D>("Textures/" + texture);
		}
	}
}
