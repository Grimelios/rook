﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rook
{
	public static class Resolution
	{
		public const int Width = 800;
		public const int Height = 600;

		public static int WindowWidth { get; set; } = 800;
		public static int WindowHeight { get; set; } = 600;

		public static bool Fullscreen { get; set; }
	}
}
