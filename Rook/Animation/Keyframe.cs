﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rook.Animation
{
	public class Keyframe
	{
		public Keyframe(int time, Transform transform)
		{
			Time = time;
			Transform = transform;
		}

		public int Time { get; }

		public Transform Transform { get; }
	}
}
