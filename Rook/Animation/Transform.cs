﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace Rook.Animation
{
	public class Transform
	{
		public static Transform Lerp(Transform t1, Transform t2, float amount)
		{
			Vector2 position = Vector2.Lerp(t1.Position, t2.Position, amount);

			float rotation = MathHelper.Lerp(t1.Rotation, t2.Rotation, amount);

			return new Transform(position, rotation);
		}

		public Transform(Vector2 position, float rotation)
		{
			Position = position;
			Rotation = rotation;
		}

		public Vector2 Position { get; }

		public float Rotation { get; }
	}
}
