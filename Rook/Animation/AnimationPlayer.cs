﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rook.Interfaces;

namespace Rook.Animation
{
	public class AnimationPlayer : IDynamic
	{
		private Skeleton parent;
		private Animation animation;

		public AnimationPlayer(Skeleton parent)
		{
			this.parent = parent;
		}

		public void Play(Animation animation, bool looped = false)
		{
			this.animation = animation;
		}

		/// <summary>
		/// Updates the class.
		/// </summary>
		public void Update(float dt)
		{
		}
	}
}
