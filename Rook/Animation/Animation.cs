﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rook.Animation
{
	public class Animation
	{
		public Animation(Timeline[] timelines, int length, int[] triggers)
		{
			Timelines = timelines;
			Length = length;
			Triggers = triggers;
		}

		public Timeline[] Timelines { get; }

		public int Length { get; }
		public int[] Triggers { get; }
	}
}
