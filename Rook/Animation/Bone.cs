﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Rook.Interfaces;

namespace Rook.Animation
{
	public class Bone
	{
		private List<Bone> children;

		public Vector2 LocalPosition { get; set; }
		public Vector2 AbsolutePosition { get; set; }

		public float LocalRotation { get; set; }
		public float AbsoluteRotation { get; set; }

		public void AddChild(Bone child)
		{
			if (children == null)
			{
				children = new List<Bone>();
			}

			children.Add(child);
		}

		public void ComputeChildTransforms()
		{
			if (children == null)
			{
				return;
			}

			foreach (Bone bone in children)
			{
				bone.AbsolutePosition = AbsolutePosition + bone.LocalPosition;
				bone.AbsoluteRotation = AbsoluteRotation + bone.LocalRotation;
				bone.ComputeChildTransforms();
			}
		}
	}
}
