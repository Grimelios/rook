﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Rook.Interfaces;
using Rook.Json;

namespace Rook.Animation
{
	using AnimationGroup = Dictionary<string, Animation>;
	using AnimationCache = Dictionary<string, Dictionary<string, Animation>>;

	public class Skeleton : IPositionable, IDynamic
	{
		private static AnimationCache animationCache = new AnimationCache();

		private Bone masterBone;
		private AnimationGroup animationGroup;
		private AnimationPlayer animationPlayer;

		public Skeleton(string group, int[][] boneHierarchy)
		{
			animationCache.TryGetValue(group, out animationGroup);
			animationPlayer = new AnimationPlayer(this);

			if (animationGroup == null)
			{
				animationGroup = JsonUtilities.Deserialize<AnimationGroup>("Animations/" + group);
				animationCache.Add(group, animationGroup);
			}

			Bones = new Bone[boneHierarchy.GetLength(0)];

			for (int i = 0; i < Bones.Length; i++)
			{
				Bones[i] = new Bone();
			}

			for (int i = 0; i < Bones.Length; i++)
			{
				Bone bone = Bones[i];

				int[] childIndices = boneHierarchy[i];

				if (childIndices == null)
				{
					continue;
				}

				foreach (int index in childIndices)
				{
					bone.AddChild(Bones[index]);
				}
			}

			// This assumes the 0th bone is the torso (or other top-level bone).
			masterBone = new Bone();
			masterBone.AddChild(Bones[0]);
		}

		public Vector2 Position
		{
			get => throw new NotImplementedException();
			set => masterBone.AbsolutePosition = value;
		}

		public Bone[] Bones { get; }

		public void Play(string animation)
		{
			animationPlayer.Play(animationGroup[animation]);
		}

		public void Update(float dt)
		{
			animationPlayer.Update(dt);
			masterBone.ComputeChildTransforms();
		}
	}
}
