﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Rook.Interfaces;

namespace Rook.Animation
{
	public class AnimationRenderer : IRenderable
	{
		private Texture2D spritesheet;
		private Skeleton parent;

		public AnimationRenderer(string spritesheet, Skeleton parent)
		{
			this.parent = parent;
			this.spritesheet = ContentLoader.LoadTexture("Spritesheets/" + spritesheet);
		}

		public void Draw(SuperBatch sb)
		{
			foreach (Bone bone in parent.Bones)
			{
				//sb.Draw(spritesheet, bone.AbsolutePosition, null, bone.AbsoluteRotation);
			}
		}
	}
}
