﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rook.Interfaces;

namespace Rook.Core
{
	public class Timer : IDynamic
	{
		// Trigger functions can return false to break a repeating timer early.
		private Action<float> tick;
		private Action<float> nonRepeatingTrigger;
		private Func<float, bool> repeatingTrigger;

		public Timer(float duration, Func<float, bool> repeatingTrigger, float initialElapsed = 0) :
			this(duration, null, repeatingTrigger, initialElapsed)
		{
		}

		public Timer(float duration, Action<float> tick, Func<float, bool> repeatingTrigger, float initialElapsed = 0)
		{
			this.tick = tick;
			this.repeatingTrigger = repeatingTrigger;

			Duration = duration;
			Elapsed = initialElapsed;
		}

		public Timer(float duration, Action<float> nonRepeatingTrigger, float initialElapsed = 0) :
			this(duration, null, nonRepeatingTrigger, initialElapsed)
		{
		}

		public Timer(float duration, Action<float> tick, Action<float> nonRepeatingTrigger, float initialElapsed = 0)
		{
			this.tick = tick;
			this.nonRepeatingTrigger = nonRepeatingTrigger;

			Duration = duration;
			Elapsed = initialElapsed;
		}

		public float Elapsed { get; private set; }
		public float Duration { get; set; }

		public bool Paused { get; set; }
		public bool Complete { get; private set; }

		public void Reset()
		{
			Elapsed = 0;
			Complete = false;
		}

		public void Reverse()
		{
			Elapsed = Duration - Elapsed;
		}

		public void Update(float dt)
		{
			if (Paused)
			{
				return;
			}

			Elapsed += dt * 1000;

			if (Elapsed >= Duration)
			{
				// This while loop is necessary to properly handle timers with duration less than a frame. It's also possible for trigger
				// functions to pause the timer.
				while (Elapsed >= Duration && !Paused)
				{
					Elapsed -= Duration;

					// Calling the tick function here avoids having to handle the final tick in the trigger function.
					if (nonRepeatingTrigger != null)
					{
						tick?.Invoke(1);
						nonRepeatingTrigger(Elapsed);

						return;
					}
					
					// Repeating trigger functions can return false to end the timer.
					if (!repeatingTrigger(Elapsed))
					{
						Complete = true;

						return;
					}
				}
			}
			else
			{
				tick?.Invoke(Elapsed / Duration);
			}
		}
	}
}
