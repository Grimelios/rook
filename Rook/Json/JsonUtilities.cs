﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Newtonsoft.Json;

namespace Rook.Json
{
	public static class JsonUtilities
	{
		public static T Deserialize<T>(string filename, bool useTypeNameHandling = false)
		{
			JsonSerializerSettings settings = new JsonSerializerSettings();

			if (useTypeNameHandling)
			{
				settings.TypeNameHandling = TypeNameHandling.Auto;
			}

			return JsonConvert.DeserializeObject<T>(File.ReadAllText(Paths.Json + filename), settings);
		}

		public static T Deserialize<T>(string filename, string extractToken) where T : class, new()
		{
			return JsonConvert.DeserializeObject<T>(File.ReadAllText(Paths.Json + filename), new JsonExtractor<T>(extractToken));
		}

		public static void Serialize(object data, string filename)
		{
			File.WriteAllText(Paths.Json + filename, JsonConvert.SerializeObject(data));
		}

		public static void Write(JsonWriter writer, Vector2 value)
		{
			writer.WriteRawValue($"\"{value.X},{value.Y}\"");
		}
	}
}
