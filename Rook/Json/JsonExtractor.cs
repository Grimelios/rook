﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Rook.Json
{
	public class JsonExtractor<T> : JsonConverter where T : class, new()
	{
		private string target;

		public JsonExtractor(string target)
		{
			this.target = target;
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			throw new NotImplementedException();
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			JObject jObject = JObject.Load(reader);
			JToken token = jObject[target];

			T item = new T();

			// This line is required to prevent recursive calls down into the same converter.
			serializer.Converters.Clear();
			serializer.Populate(token.CreateReader(), item);

			return item;
		}

		public override bool CanConvert(Type objectType)
		{
			return true;
		}
	}
}
