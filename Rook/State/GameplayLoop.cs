﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework;
using Rook.Entities.Bosses;
using Rook.Entities.Core;
using Rook.Physics;

namespace Rook.State
{
	public class GameplayLoop : GameLoop
	{
		private Scene scene;
		private PhysicsAccumulator accumulator;

		public override void Initialize(Camera camera)
		{
			World world = new World(new Vector2(0, PhysicsConstants.Gravity));
			PhysicsFactory.Initialize(world);

			accumulator = new PhysicsAccumulator(world);
			scene = new Scene();
			scene.Add(0, new SwordBoss());
		}

		public override void Update(float dt)
		{
			scene.Update(dt);
			accumulator.Update(dt);
		}

		public override void Draw(SuperBatch sb)
		{
			sb.Begin(Coordinates.World);
			scene.Draw(sb);
			sb.End();
		}
	}
}
