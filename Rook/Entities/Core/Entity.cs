﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FarseerPhysics.Common;
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework;
using Rook.Core;
using Rook.Interfaces;
using Rook.Physics;

namespace Rook.Entities.Core
{
	public enum EntityTypes
	{
		Boss,
		Player,
		World,

		// This type should be used for entities not directly managed by the scene.
		None
	}

	public abstract class Entity : IPositionable, IRotatable, IBoundable, IDynamic, IRenderable, IDisposable
	{
		private Vector2 position;

		private float rotation;

		private Body body;
		private List<Component2D> components;
		private List<Timer> timers;

		private bool selfUpdate;

		protected Entity(EntityTypes type = EntityTypes.None)
		{
			EntityType = type;
			Bounds = new Bounds();
		}

		protected List<Component2D> Components => components ?? (components = new List<Component2D>());
		protected List<Timer> Timers => timers ?? (timers = new List<Timer>());

		protected bool UseCustomComponentPositioning { get; set; }
		protected bool Centered { get; set; }
		protected bool SuppressBodyPositioning { get; set; }

		// This value is public because of run controllers.
		public bool OnGround { get; set; }

		public virtual Vector2 Position
		{
			get => position;
			set
			{
				PreviousPosition = position;
				position = value;

				Point point = value.ToPoint();

				if (Centered)
				{
					Bounds.Center = point;
				}
				else
				{
					Bounds.Position = point;
				}

				if (Body != null && !selfUpdate && !SuppressBodyPositioning)
				{
					Body.Position = PhysicsConvert.ToMeters(value);
				}

				if (!UseCustomComponentPositioning)
				{
					components?.ForEach(c => c.Position = value);
				}
			}
		}

		public Vector2 PreviousPosition { get; private set; }

		public virtual Vector2 Velocity
		{
			get => PhysicsConvert.ToPixels(Body.LinearVelocity);
			set => Body.LinearVelocity = PhysicsConvert.ToMeters(value);
		}

		public virtual float Rotation
		{
			get => rotation;
			set
			{
				rotation = value;

				if (Body != null && !selfUpdate)
				{
					Body.Rotation = value;
				}

				components?.ForEach(c => c.Rotation = value);
			}
		}

		public Bounds Bounds { get; protected set; }

		public Body Body
		{
			get => body;
			set
			{
				body = value;
				body.OnCollision += (fixtureA, fixtureB, contact) =>
				{
					Body otherBody = fixtureB.Body;
					Vector2 normal;
					FixedArray2<Vector2> points;
					
					contact.GetWorldManifold(out normal, out points);

					return OnCollision(otherBody, otherBody.UserData as Entity, fixtureB.UserData, PhysicsConvert.ToPixels(points[0]),
						normal);
				};
			}
		}

		public Scene Scene { get; set; }

		public EntityTypes EntityType { get; }

	    protected virtual bool OnCollision(Body body, Entity entity, object fixtureData, Vector2 position, Vector2 normal)
	    {
			// All entities should collide by default.
	        return true;
	    }

        public virtual void Dispose()
		{
		}

		public virtual void Update(float dt)
		{
			if (Body != null)
			{
				if (OnGround)
				{
					Body.Position = PhysicsConvert.ToMeters(Position);
					Body.Rotation = Rotation;
				}
				else
				{
					selfUpdate = true;
					Position = PhysicsConvert.ToPixels(Body.Position);
					Rotation = Body.Rotation;
					selfUpdate = false;
				}
			}

			timers?.ForEach(t => t.Update(dt));
			components?.ForEach(c => c.Update(dt));
		}

		public virtual void Draw(SuperBatch sb)
		{
			components?.ForEach(c => c.Draw(sb));
		}
	}
}
