﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rook.Data.Bosses;

namespace Rook.Entities.Bosses
{
	public class SwordBoss : Boss
	{
		private SwordBossData data;

		public SwordBoss() : base(BossTypes.SwordBoss)
		{
		}

		protected override void Initialize(BossData data)
		{
			this.data = (SwordBossData)data;
		}

		protected override void AI(float dt)
		{
		}

		private void PlatformAttack()
		{
		}
	}
}
