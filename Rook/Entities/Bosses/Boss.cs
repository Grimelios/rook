﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rook.Data.Bosses;
using Rook.Entities.Core;
using Rook.Json;

namespace Rook.Entities.Bosses
{
	public enum BossTypes
	{
		SwordBoss
	}

	public abstract class Boss : LivingEntity
	{
		private static BossData[] dataArray = JsonUtilities.Deserialize<BossData[]>("Bosses.json", true);

		protected Boss(BossTypes type)
		{
			Initialize(dataArray[(int)type]);
		}

		protected virtual void Initialize(BossData data)
		{

		}

		protected abstract void AI(float dt);

		public override void Update(float dt)
		{
			AI(dt);

			base.Update(dt);
		}
	}
}
