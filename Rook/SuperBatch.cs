﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Rook
{
	public enum Coordinates
	{
		Screen,
		World
	}

	public class SuperBatch
	{
		private Camera camera;
		private SpriteBatch sb;
		private GraphicsDevice graphicsDevice;

		public SuperBatch(Camera camera, SpriteBatch sb, GraphicsDevice graphicsDevice)
		{
			this.camera = camera;
			this.sb = sb;
			this.graphicsDevice = graphicsDevice;
		}

		public void Begin(Coordinates coordinates)
		{
			Matrix transform = coordinates == Coordinates.Screen ? Matrix.Identity : camera.Transform;

			sb.Begin(SpriteSortMode.Deferred, null, null, null, null, null, transform);
		}

		public void End()
		{
			sb.End();
		}

		public void Draw(Texture2D texture, Vector2 position, Rectangle sourceRect)
		{
			sb.Draw(texture, position, sourceRect, Color.White);
		}

		public void DrawString(SpriteFont font, string value, Vector2 position, Vector2 origin, Color color, float rotation, float scale)  
		{
			sb.DrawString(font, value, position, color, rotation, origin, scale, SpriteEffects.None, 0);
		}
	}
}
