﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Rook.Json;

namespace Rook.Physics.Shapes
{
	[JsonConverter(typeof(EdgeCollectionConverter))]
	public class EdgeCollection
	{
		public EdgeCollection(Vector2[] points)
		{
			// This assumes at least two points (for at least one valid edge).
			Edges = new Edge[points.Length - 1];

			int edgeCount = Edges.Length;

			for (int i = 0; i < points.Length - 1; i++)
			{
				Edges[i] = new Edge(points[i], points[i + 1]);
			}

			if (edgeCount > 1)
			{
				Edge first = Edges[0];
				Edge last = Edges[edgeCount - 1];

				first.Next = Edges[1];
				last.Previous = Edges[edgeCount - 2];

				bool looped = last.End == first.Start;

				if (looped)
				{
					first.Previous = last;
					last.Next = first;
				}
			}

			for (int i = 1; i < edgeCount - 1; i++)
			{
				Edges[i].Next = Edges[i + 1];
				Edges[i].Previous = Edges[i - 1];
			}
		}

		public Edge[] Edges { get; }

		private class EdgeCollectionConverter : ClassConverter<EdgeCollection>
		{
			public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
			{
				Edge[] edges = ((EdgeCollection)value).Edges;

				writer.WriteStartObject();
				writer.WritePropertyName("Points");
				writer.WriteStartArray();

				foreach (Edge edge in edges)
				{
					JsonUtilities.Write(writer, edge.Start);
				}
				
				JsonUtilities.Write(writer, edges.Last().End);

				writer.WriteEndArray();
				writer.WriteEndObject();
			}

			public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
			{
				JObject jObject = JObject.Load(reader);
				JArray array = (JArray)jObject["Points"];

				Vector2[] points = array.Select(v => ParseFunctions.ParseVector2(v.ToString())).ToArray();

				return new EdgeCollection(points);
			}
		}
	}
}
