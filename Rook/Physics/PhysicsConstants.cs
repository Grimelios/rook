﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rook.Physics
{
	public static class PhysicsConstants
	{
		public const int PixelsPerMeter = 48;
		public const int Gravity = 25;
	}
}
